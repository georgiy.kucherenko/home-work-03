package com.example.notebook.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)//если принимаем лишние - чтоб мы сюда не добавляли
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Phone {
    Long id;
    String phoneNumber;
}
