package com.example.notebook.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)//если принимаем лишние - чтоб мы сюда не добавляли
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@Getter
@Setter
@AllArgsConstructor
public class Contactt {
    Long id;
    String name;
    List<Phone> phoneList;
    List<Email> emailList;
    User user;
}
