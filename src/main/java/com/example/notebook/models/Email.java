package com.example.notebook.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)//если принимаем лишние - чтоб мы сюда не добавляли
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Email {
    Long id;
    String emailAddress;
}
