package com.example.notebook.models.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmailRepository extends JpaRepository<EmailEntity, Long> {
    @Query(nativeQuery = true,
            value = "select count(*) from email e where e.contacts_id=?1")
    Long countEmailEntitiesByContactsId(Long intValue);
}
