package com.example.notebook.models.jpa;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "email")
public class EmailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String emailAddress;
}
