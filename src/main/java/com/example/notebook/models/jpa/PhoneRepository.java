package com.example.notebook.models.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PhoneRepository extends JpaRepository<PhoneEntity, Long> {

    @Query(nativeQuery = true,
            value = "select count(*) from phone p where p.contacts_id =?1")
    Long countPhoneEntitiesByContactsId(Integer contactId);
}
