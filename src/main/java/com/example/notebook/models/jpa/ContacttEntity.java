package com.example.notebook.models.jpa;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "contacts")
public class ContacttEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "contacts_id")//, mappedBy = "phoneEntityList"
    private List<PhoneEntity> phoneEntityList;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "contacts_id")//, mappedBy = "phoneEntityList"
    private List<EmailEntity> emailEntityList;
    @OneToOne
    private UserEntity userEntity;
}
