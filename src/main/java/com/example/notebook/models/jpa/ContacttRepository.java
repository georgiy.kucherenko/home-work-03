package com.example.notebook.models.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ContacttRepository extends JpaRepository<ContacttEntity, Long>, JpaSpecificationExecutor<ContacttEntity> {

    List<ContacttEntity> findByUserEntity_Id(Long id);

    Page<ContacttEntity> findAllByUserEntity_Id(Long id, Pageable pageable);

    void deleteById(Long id);

    Page<ContacttEntity> findAllByNameContainingIgnoreCaseAndUserEntity_Id(String name, Long id, Pageable pageable);

    //     Альтернативный поиск по почте и айди пользователя
    @Query(nativeQuery = true,
            value = "select distinct c.id, c.name, c.person_entity_id from contacts c right outer join email e on c.id=e.contacts_id where e.email_address = ?1 and c.person_entity_id=?2",
            countQuery = "SELECT distinct count(*) FROM contacts c right outer join email e on c.id=e.contacts_id where e.email_address = ?1")
    Page<ContacttEntity> findByEmailAndPersonId(String email, Long personId, Pageable pageable);

    //    Альтернативный поиск по имени контакта и айди пользователя
    Page<ContacttEntity> findAllByUserEntity_IdAndNameLikeIgnoreCase(Long id, String name, Pageable pageable);

    Optional<ContacttEntity> findById(Long id);

    Page<ContacttEntity> findDistinctByUserEntity_IdAndPhoneEntityListNotNull(Long personEntityId, Pageable pageable);

    Page<ContacttEntity> findDistinctByUserEntity_IdAndEmailEntityListNotNull(Long personId, Pageable pageable);


}
