package com.example.notebook.exceptions;

public class AuthentificationRegistrationException extends Exception {
    public AuthentificationRegistrationException(String message) {
        super(message);
    }
}
