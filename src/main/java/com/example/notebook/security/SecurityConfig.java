package com.example.notebook.security;

import com.example.notebook.exceptions.AuthentificationRegistrationException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import java.io.IOException;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth ->
                        {
                            try {
                                auth.requestMatchers("/").permitAll()
                                        .requestMatchers("/registration").permitAll()
                                        .requestMatchers("/loginfail").permitAll()
                                        .requestMatchers("/register").permitAll()
                                        .requestMatchers("/index").permitAll()
                                        .anyRequest().authenticated()
                                        .and().formLogin()
                                        .loginPage("/login")
                                        .loginProcessingUrl("/login")
                                        .defaultSuccessUrl("/main", true)
                                        .failureUrl("/loginfail")
                                        .and()
                                        .logout()
                                        .logoutUrl("/custom-logout")
                                        .invalidateHttpSession(true)
                                        .deleteCookies()
                                        .logoutSuccessUrl("/")
                                        .and().httpBasic((basic) -> basic
                                                .addObjectPostProcessor(new ObjectPostProcessor<BasicAuthenticationFilter>() {
                                                    @Override
                                                    public <O extends BasicAuthenticationFilter> O postProcess(O filter) {
                                                        filter.setSecurityContextRepository(new HttpSessionSecurityContextRepository());
                                                        return filter;
                                                    }
                                                })
                                        );
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                )
                .csrf().disable();
        return http.build();
    }

}
