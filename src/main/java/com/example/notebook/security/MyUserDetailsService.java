package com.example.notebook.security;

import com.example.notebook.models.User;
import com.example.notebook.models.jpa.UserEntity;
import com.example.notebook.models.jpa.UserRepository;
import com.example.notebook.util.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MyUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userMapper.toDto(userRepository.findByLogin(username));
        if (user == null) {
            throw new UsernameNotFoundException("not found " + username);
        }

        return new SecuredUser(
                user.getLogin(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                List.of((GrantedAuthority) () -> "user"),
                user.getId());
    }
}
