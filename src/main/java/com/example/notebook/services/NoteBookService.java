package com.example.notebook.services;

import com.example.notebook.exceptions.ElementNotFoundException;
import com.example.notebook.exceptions.IllegalInputException;
import com.example.notebook.models.Contactt;
import com.example.notebook.models.jpa.*;
import com.example.notebook.util.*;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;


import java.util.Optional;

@RequiredArgsConstructor
public class NoteBookService {
    private final ContacttRepository contacttRepository;
    private final EmailRepository emailRepository;
    private final UserRepository userRepository;
    private final PhoneRepository phoneRepository;
    private final ContactsMapper contactsMapper;

    public void addNewContact(WrapperContact wrapperOfContacts, Long userId) {
        Contactt unwrapedContact = WrapperContact.unWrap(wrapperOfContacts);
        if (unwrapedContact.getName().trim().equals("")) {
            throw new IllegalInputException("Name of contact cannot be null");
        }
        ContacttEntity contacttEntity = contactsMapper.toEntity(unwrapedContact);
        contacttEntity.setUserEntity(
                userRepository.findById(userId).orElseThrow(() -> new ElementNotFoundException("couldn't find associated person"))
        );
        contacttRepository.save(contacttEntity);
    }

    public Page<Contactt> findAllContactsByPersonIdPageable(Long id, Pageable pageable) {
        Page<ContacttEntity> byPersonEntityId = contacttRepository.findAllByUserEntity_Id(id, pageable);
        if (byPersonEntityId.getContent().size() == 0) {
            throw new ElementNotFoundException("your contact list is empty");
        }
        return byPersonEntityId.map(contactsMapper::toDto);
    }

    public void deleteContactById(Long contactId) {
        contacttRepository.deleteById(contactId);
    }

    public WrapperContact findContacttById(Long id) {
        Optional<ContacttEntity> byId = contacttRepository.findById(id);
        Contactt contactt = contactsMapper.toDto(byId.orElseThrow(() -> new ElementNotFoundException("no elemnt with such id")));
        return new WrapperContact(contactt);
    }

    public Page<Contactt> findByPersonIdAndNameIgnoreCaseLike(Long id, String name, Pageable pageable) {
        Page<ContacttEntity> resultPages = contacttRepository.findAllByNameContainingIgnoreCaseAndUserEntity_Id(
                name, id, pageable
        );
        return resultPages.map(contactsMapper::toDto);
    }

    public Page<Contactt> findAllByEmailContainingAndPersonId2(Long id, String email, Pageable pageable) {
        Specification<ContacttEntity> finalSpecification = Specification.allOf(
                createSpecificationByEmail(email), createSpecificationByPersonId(id)
        );
        Page<ContacttEntity> resultPage = contacttRepository.findAll(finalSpecification, pageable);
        return resultPage.map(contactsMapper::toDto);
    }

    private Specification<ContacttEntity> createSpecificationByPersonId(Long ids) {
        return (root, query, criteriaBuilder) -> {
            Join<UserEntity, ContacttEntity> personContact = root.join("userEntity");
            return criteriaBuilder.equal(personContact.get("id"), ids);
        };
    }

    private Specification<ContacttEntity> createSpecificationByEmail(String emails) {
        return (root, query, criteriaBuilder) -> {
            Join<EmailEntity, ContacttEntity> emailsOfContact = root.join("emailEntityList", JoinType.RIGHT);
            return criteriaBuilder.equal(emailsOfContact.get("emailAddress"), emails);
        };
    }

    public Page<Contactt> findAllByPhoneContainingAndPersonId2(Long id, String phone, Pageable pageable) {
        Specification<ContacttEntity> finalSpecification = Specification.allOf(
                createSpecificationByPhone(phone), createSpecificationByPersonId(id)
        );
        Page<ContacttEntity> resultPage = contacttRepository.findAll(finalSpecification, pageable);
        return resultPage.map(contactsMapper::toDto);
    }

    private Specification<ContacttEntity> createSpecificationByPhone(String phone) {
        return (root, query, criteriaBuilder) -> {
            Join<PhoneEntity, ContacttEntity> phonesOfContact = root.join("phoneEntityList", JoinType.RIGHT);
            return criteriaBuilder.equal(phonesOfContact.get("phoneNumber"), phone);
        };
    }

    public void saveOrUpdate(Contactt unWrap, Long personId) {
        ContacttEntity contacttEntity = contactsMapper.toEntity(unWrap);
        contacttEntity.setUserEntity(userRepository.findById(personId)
                .orElseThrow(() -> new ElementNotFoundException("no such person exists")));
        contacttRepository.save(contacttEntity);
    }

    public void deletePhoneByIdIfNotLast(Long contactId, Long phoneId, Long personId) {
        Long quantityOfPhones = phoneRepository.countPhoneEntitiesByContactsId((Long.valueOf(contactId)).intValue());

        if (quantityOfPhones > 1) {
            phoneRepository.deleteById(phoneId);
        } else {
            throw new IllegalInputException("cannot delete last element. At least one phone must be present");
        }
    }

    public void deleteEmailByIdIfNotLast(Long contactId, Long emailId, Long personId) {
        Long quantityOfEmails = emailRepository.countEmailEntitiesByContactsId(contactId);

        if (quantityOfEmails > 1) {
            emailRepository.deleteById(emailId);
        } else {
            throw new IllegalInputException("cannot delete last element. At least one email must be present");
        }
    }

    public Page<Contactt> findContacttsWithPhones(Long personId, Pageable pageable) {
        Page<ContacttEntity> resultPage = contacttRepository.findDistinctByUserEntity_IdAndPhoneEntityListNotNull(
                personId,
                pageable
        );
        return resultPage.map(contactsMapper::toDto);
    }

    Specification<ContacttEntity> createSpecificationForContactsHavingPhones() {
        return (root, query, criteriaBuilder) -> criteriaBuilder
                .isNotNull(root.get("phoneEntityList"));
    }

    public Page<Contactt> findContacttsWithEmails(long personId, Pageable pageable) {
        Page<ContacttEntity> contacttEntities = contacttRepository
                .findDistinctByUserEntity_IdAndEmailEntityListNotNull(personId, pageable);
        return contacttEntities.map(contactsMapper::toDto);
    }
}
