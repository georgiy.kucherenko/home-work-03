package com.example.notebook.services;

import com.example.notebook.models.User;
import com.example.notebook.models.jpa.UserRepository;
import com.example.notebook.util.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public void register(String name, String pass) {
        User user = new User(null, name, passwordEncoder.encode(pass));
        userRepository.save(userMapper.toEntity(user));
    }
}
