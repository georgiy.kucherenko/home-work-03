package com.example.notebook.controllers;

import com.example.notebook.exceptions.AuthentificationRegistrationException;
import com.example.notebook.exceptions.ElementNotFoundException;
import com.example.notebook.exceptions.IllegalInputException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(IllegalInputException.class)
    String onIllegalInputException(IllegalInputException illegalInputException, Model model) {
        model.addAttribute("exceptionmessage", illegalInputException.getMessage());
        return "exceptionview";
    }

    @ExceptionHandler(ElementNotFoundException.class)
    String onElementNotFoundException(ElementNotFoundException elementNotFoundException, Model model) {
        model.addAttribute("exceptionmessage", elementNotFoundException.getMessage());
        return "exceptionview";
    }

    @ExceptionHandler(AuthentificationRegistrationException.class)
    String onAuthentificationException(AuthentificationRegistrationException auth, Model model) {
        model.addAttribute("exceptionmessage", auth.getMessage());
        return "authexceptionview";
    }
}
