package com.example.notebook.controllers;

import com.example.notebook.exceptions.IllegalInputException;
import com.example.notebook.models.*;
import com.example.notebook.security.SecuredUser;
import com.example.notebook.services.NoteBookService;
import com.example.notebook.util.WrapperContact;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@Log4j2
@RequiredArgsConstructor
public class ContactsController {
    private final NoteBookService noteBookService;

    Long getUserIdFromContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecuredUser securedUser = (SecuredUser) authentication.getPrincipal();
        log.info("secured user {}", securedUser);
        return securedUser.getUserID();
    }

    @GetMapping("/addcontact")
    ModelAndView openAddContact() {
        ModelAndView emptyTemplate = new ModelAndView("addcontact");
        emptyTemplate.addObject("wrapperOfContacts", new WrapperContact());
        return emptyTemplate;
    }

    @PostMapping("/addcontact")
    String addContact(@ModelAttribute WrapperContact wrapperOfContacts) {
        noteBookService.addNewContact(wrapperOfContacts, getUserIdFromContext());
        return "main";
    }

    @GetMapping("/main")
    String justOpen() {
        return "main";
    }

    @GetMapping("/showallcontacts/{pagenumber}")
    String showAllContacts(@PathVariable String pagenumber, Model model) {
        Pageable pageable = PageRequest.of(Integer.parseInt(pagenumber), 2);
        Page<Contactt> resultPage = noteBookService.findAllContactsByPersonIdPageable(getUserIdFromContext(), pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/showallcontacts/");
        return "resultsofsearch";
    }

    @GetMapping("/deletecontact/{id}")
    String deleteContact(@PathVariable String id, HttpServletRequest req) {
        noteBookService.deleteContactById(Long.parseLong(id));
        String referer = req.getHeader("Referer");
        return "redirect:" + referer;
    }

    @GetMapping("/showcontact/{id}")
    String showContact(@PathVariable String id, Model model) {
        WrapperContact contacttById = noteBookService.findContacttById(Long.parseLong(id));
        log.info("^^^^^^^^^^^^^ wraped Contact {}", contacttById);
        model.addAttribute("wrapperOfContacts", contacttById);
        return "showcontact";
    }

    @GetMapping("/findbyname/{pagenumber}")
    String findByName(@RequestParam(defaultValue = "") String contactsname, @PathVariable String pagenumber, Model model, @RequestParam(defaultValue = "") String searchedname) {
        Pageable pageable = PageRequest.of(Integer.parseInt(pagenumber), 2);
        if (searchedname.trim().equals("") && contactsname.trim().equals("")) {
            throw new IllegalInputException("name field cannot be empty");
        }
        if (contactsname.trim().equals("")) contactsname = searchedname;
        Page<Contactt> resultPage = noteBookService.findByPersonIdAndNameIgnoreCaseLike(getUserIdFromContext(), contactsname, pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/findbyname/");
        model.addAttribute("searchedname", contactsname);
        return "resultsofsearch";
    }

    @GetMapping("/findbyemail/{pagenumber}")
    String findByEmail(@RequestParam(defaultValue = "") String email, @PathVariable String pagenumber, Model model, @RequestParam(defaultValue = "") String searchedname) {
        Pageable pageable = PageRequest.of(Integer.parseInt(pagenumber), 2);
        if (email.equals("")) email = searchedname;
        Page<Contactt> resultPage = noteBookService.findAllByEmailContainingAndPersonId2(getUserIdFromContext(), email, pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/findbyemail/");
        model.addAttribute("searchedname", email);
        return "resultsofsearch";
    }

    @GetMapping("/findbyphone/{pagenumber}")
    String findByPhone(@RequestParam(defaultValue = "") String phone, @PathVariable String pagenumber, Model model, @RequestParam(defaultValue = "") String searchedname) {
        Pageable pageable = PageRequest.of(Integer.parseInt(pagenumber), 2);
        if (phone.equals("")) phone = searchedname;
        Page<Contactt> resultPage = noteBookService.findAllByPhoneContainingAndPersonId2(getUserIdFromContext(), phone, pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/findbyphone/");
        model.addAttribute("searchedname", phone);
        return "resultsofsearch";
    }

    @PostMapping("/editcontact")
    String editContact(WrapperContact wrapperOfContacts, Model model) {
        model.addAttribute("wrapperOfContacts", wrapperOfContacts);
        return "editthecontact";
    }

    @PostMapping("/savecontactedit")
    String saveEditedContact(WrapperContact wrapperOfContacts) {
        noteBookService.saveOrUpdate(WrapperContact.unWrap(wrapperOfContacts), getUserIdFromContext());
        return "main";
    }

    @GetMapping("/delete/{contactId}/phone/{phoneId}")
    String deletePhoneFromContact(@PathVariable Long contactId, @PathVariable Long phoneId, HttpServletRequest request) {
        String referer = request.getHeader("Referer");
        noteBookService.deletePhoneByIdIfNotLast(contactId, phoneId, getUserIdFromContext());
        return "redirect:" + referer;
    }

    @GetMapping("/delete/{contactId}/email/{emailId}")
    String deleteEmailFromContact(@PathVariable Long contactId, @PathVariable Long emailId, HttpServletRequest request) {
        String referer = request.getHeader("Referer");
        noteBookService.deleteEmailByIdIfNotLast(contactId, emailId, getUserIdFromContext());
        return "redirect:" + referer;
    }

    @GetMapping("/findcontainingphones/{pageNumber}")
    String findContainingPhones(@PathVariable int pageNumber, Model model) {
        Pageable pageable = PageRequest.of(pageNumber, 2);
        Page<Contactt> resultPage = noteBookService.findContacttsWithPhones(getUserIdFromContext(), pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/findcontainingphones/");
        return "resultsofsearch";
    }

    @GetMapping("/findcontainingemails/{pageNumber}")
    String findContainingEmails(@PathVariable int pageNumber, Model model) {
        Pageable pageable = PageRequest.of(pageNumber, 2);
        Page<Contactt> resultPage = noteBookService.findContacttsWithEmails(getUserIdFromContext(), pageable);
        model.addAttribute("pageofcontacts", resultPage);
        model.addAttribute("linkto", "/findcontainingemails/");
        return "resultsofsearch";
    }
}
