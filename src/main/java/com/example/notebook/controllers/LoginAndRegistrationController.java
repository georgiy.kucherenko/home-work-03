package com.example.notebook.controllers;

import com.example.notebook.exceptions.AuthentificationRegistrationException;
import com.example.notebook.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Slf4j
@RequiredArgsConstructor
public class LoginAndRegistrationController {
    private final UserService userService;

    @GetMapping("/loginfail")
    String onFailLogin() {
        return "loginfail";
    }

    @GetMapping("/registration")
    String goToRegistration() {
        return "registrationform";
    }

    @PostMapping("/register")
    String registerUser(@RequestParam("login") String login, @RequestParam("pass") String pass, @RequestParam("pass2") String pass2) throws AuthentificationRegistrationException {
        if (!pass.equals(pass2)) throw new AuthentificationRegistrationException("подтверждение пароля не верно");
        if (login == null || login.trim().equals("") || pass == null || pass.trim().equals(""))
            throw new AuthentificationRegistrationException("некорректные данные регистрации");
        userService.register(login, pass);
        return "index";
    }
}
