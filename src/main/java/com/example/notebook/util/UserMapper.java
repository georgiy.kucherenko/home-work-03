package com.example.notebook.util;

import com.example.notebook.models.User;
import com.example.notebook.models.jpa.UserEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserMapper implements DaoMapper<User, UserEntity> {

    @Override
    public User toDto(UserEntity userEntity) {
        User user = new User(
                userEntity.getId(),
                userEntity.getLogin(),
                userEntity.getPassword());
        return user;
    }

    @Override
    public UserEntity toEntity(User user) {
        UserEntity userEntity = new UserEntity();
        if (user.getId() != null) {
            userEntity.setId(user.getId());
        }
        userEntity.setPassword(user.getPassword());
        userEntity.setLogin(user.getLogin());
        return userEntity;
    }
}
