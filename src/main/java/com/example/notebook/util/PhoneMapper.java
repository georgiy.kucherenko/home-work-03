package com.example.notebook.util;

import com.example.notebook.models.Phone;
import com.example.notebook.models.jpa.PhoneEntity;

public class PhoneMapper implements DaoMapper<Phone, PhoneEntity> {

    @Override
    public Phone toDto(PhoneEntity phoneEntity) {
        return new Phone(phoneEntity.getId(), phoneEntity.getPhoneNumber());
    }

    @Override
    public PhoneEntity toEntity(Phone phone) {
        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setId(phone.getId());
        phoneEntity.setPhoneNumber(phone.getPhoneNumber());
        return phoneEntity;
    }
}
