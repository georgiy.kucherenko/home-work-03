package com.example.notebook.util;

import com.example.notebook.models.Contactt;
import com.example.notebook.models.jpa.ContacttEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ContactsMapper implements DaoMapper<Contactt, ContacttEntity> {

    private final EmailMapper emailMapper;
    private final PhoneMapper phoneMapper;
    private final UserMapper userMapper;

    @Override
    public Contactt toDto(ContacttEntity contacttEntity) {
        return new Contactt(contacttEntity.getId(),
                contacttEntity.getName(),
                phoneMapper.toDto(contacttEntity.getPhoneEntityList()),
                emailMapper.toDto(contacttEntity.getEmailEntityList()),
                userMapper.toDto(contacttEntity.getUserEntity()));
    }

    @Override
    public ContacttEntity toEntity(Contactt contactt) {
        ContacttEntity contacttEntity = new ContacttEntity();
        contacttEntity.setId(contactt.getId());
        contacttEntity.setName(contactt.getName());
        contacttEntity.setPhoneEntityList(phoneMapper.toEntity(contactt.getPhoneList()));
        contacttEntity.setEmailEntityList(emailMapper.toEntity(contactt.getEmailList()));
        if (contactt.getUser() != null) {
            contacttEntity.setUserEntity(userMapper.toEntity(contactt.getUser()));
        }
        return contacttEntity;
    }
}
