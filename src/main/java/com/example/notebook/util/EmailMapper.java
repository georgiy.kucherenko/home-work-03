package com.example.notebook.util;

import com.example.notebook.models.Email;
import com.example.notebook.models.jpa.EmailEntity;

public class EmailMapper implements DaoMapper<Email, EmailEntity> {

    @Override
    public Email toDto(EmailEntity emailEntity) {
        return new Email(emailEntity.getId(), emailEntity.getEmailAddress());
    }

    @Override
    public EmailEntity toEntity(Email email) {
        EmailEntity emailEntity = new EmailEntity();
        emailEntity.setId(email.getId());
        emailEntity.setEmailAddress(email.getEmailAddress());
        return emailEntity;
    }
}
