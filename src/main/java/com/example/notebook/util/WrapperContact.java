package com.example.notebook.util;

import com.example.notebook.models.Contactt;
import com.example.notebook.models.Email;
import com.example.notebook.models.Phone;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Artur, this class is used for receiving Objects from Themleaf.
 * I found this method in internet. I guess you know such a method.
 * It is used because one cannot just forward a List of something through Themleaf.
 * We must wrap list in some class.
 * Note: this class needs empty constructor for Themleaf
 */
@Getter
@Setter
@ToString
public class WrapperContact {
    Long id;
    String contact;
    List<Phone> phoneList = new ArrayList<>();
    List<Email> emailList = new ArrayList<>();

    public WrapperContact() {
        for (int i = 0; i < 3; i++) {
            phoneList.add(new Phone(null, ""));
            emailList.add(new Email(null, ""));
        }
    }

    public WrapperContact(Contactt inputContact) {
        this.id = inputContact.getId();
        this.contact = inputContact.getName();
        this.phoneList = inputContact.getPhoneList();
        this.emailList = inputContact.getEmailList();

    }

    public static Contactt unWrap(WrapperContact wrapperContact) {
        List<Phone> phoneList2 = wrapperContact.getPhoneList().stream()
                .filter(a -> !a.getPhoneNumber().equals(""))
                .collect(Collectors.toList());
        List<Email> emailList2 = wrapperContact.getEmailList().stream()
                .filter(a -> !a.getEmailAddress().equals(""))
                .collect(Collectors.toList());
        return new Contactt(wrapperContact.getId(), wrapperContact.getContact(), phoneList2, emailList2, null);
    }
}
