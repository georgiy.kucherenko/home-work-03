package com.example.notebook.configurations;

import com.example.notebook.models.jpa.ContacttRepository;
import com.example.notebook.models.jpa.EmailRepository;
import com.example.notebook.models.jpa.UserRepository;
import com.example.notebook.models.jpa.PhoneRepository;
import com.example.notebook.services.NoteBookService;
import com.example.notebook.util.ContactsMapper;
import com.example.notebook.util.EmailMapper;
import com.example.notebook.util.UserMapper;
import com.example.notebook.util.PhoneMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelsConfiguration {

    @Bean
    ContactsMapper contactsMapper(EmailMapper emailMapper, PhoneMapper phoneMapper, UserMapper userMapper) {
        return new ContactsMapper(emailMapper, phoneMapper, userMapper);
    }

    @Bean
    UserMapper personMapper() {
        return new UserMapper();
    }

    @Bean
    EmailMapper emailMapper() {
        return new EmailMapper();
    }

    @Bean
    PhoneMapper phoneMapper() {
        return new PhoneMapper();
    }

    @Bean
    NoteBookService noteBookService(
            ContacttRepository contacttRepository,
            EmailRepository emailRepository,
            UserRepository userRepository,
            PhoneRepository phoneRepository,
            ContactsMapper contactsMapper
    ) {
        return new NoteBookService(
                contacttRepository,
                emailRepository,
                userRepository,
                phoneRepository,
                contactsMapper
        );
    }
}
